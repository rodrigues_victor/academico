package com.sicredi.academico.controller;

import com.sicredi.academico.models.Estudante;
import com.sicredi.academico.service.EstudanteService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;


@WebMvcTest(value = EstudanteController.class)
class EstudanteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EstudanteService estudanteService;


    List<Estudante> estudanteList = new ArrayList<Estudante>();
    Estudante estudanteExample;

    @Test
    public void verTodosEstudantesTest() throws Exception {

        Mockito.when(
                estudanteService.getAll()
        ).thenReturn(estudanteList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/estudande"
        ).accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expected = "[]";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), true);

    }

    @Test
    public void salvarEstudanteTest() throws Exception {

        String estudandeJson = "{\"nome\":\"jonas\",\"endereco\":\"lugarNenhum\",\"numeroDocumento\": 12.0}";
        estudanteExample = new Estudante("JONAS", "LUGARNENHUM", 12.0);

        Mockito.when(
                estudanteService.salvarEstudante(Mockito.any(Estudante.class))
        ).thenReturn(estudanteExample);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/estudande/cadastrar")
                .accept(MediaType.APPLICATION_JSON)
                .content(estudandeJson).contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        String expected = "{\"nome\":\"JONAS\",\"endereco\":\"LUGARNENHUM\",\"numeroDocumento\":12.0}";

        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);

    }

//     @Test
//     public void buscarEstudantePorMatricula() throws Exception {
//         estudanteExample = Mockito.mock(Estudante.class);

//         Mockito.when(
//                 estudanteService.buscaPorMatricula(1L)
//         ).thenReturn(estudanteExample);


//         RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
//                 "/matricula/1"
//         ).accept(MediaType.APPLICATION_JSON);

//         MvcResult result = mockMvc.perform(requestBuilder).andReturn();

//     }

}