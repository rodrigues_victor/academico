package com.sicredi.academico.service;

import com.sicredi.academico.models.Estudante;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import javax.persistence.NoResultException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest()
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class EstudanteServiceTest {

    @Autowired
    EstudanteService estudanteService;

    @Test
    void salvarEstudanteTest() {
        Estudante estudante1 = new Estudante("jonas", "lugarNenhum", 1.0);
        Estudante estudante2 = new Estudante("tavares", "lugarNenhum", 2.0);
        estudanteService.salvarEstudante(estudante1);
        Estudante estudanteSalvo = estudanteService.salvarEstudante(estudante2);
        assertEquals(2, estudanteSalvo.getMatricula());
    }

    @Test
    void salvarEstudanteComMatriculaRepetidaDeveRetornarErroTest() {
        Estudante estudante1 = new Estudante("jonas", "lugarNenhum", 1.0);
        Estudante estudante2 = new Estudante("tavares", "lugarNenhum", 1.0);
        estudanteService.salvarEstudante(estudante1);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            estudanteService.salvarEstudante(estudante2);
        });

        assertEquals(IllegalArgumentException.class, exception.getClass());
    }

    @Test
    void buscaPorMatriculaDeveRetornarEstudanteValidoTest() {
        Estudante estudanteSalvar = new Estudante("bob", "lugarNenhum", 1.0);
        estudanteService.salvarEstudante(estudanteSalvar);
        Estudante estudanteBuscado = estudanteService.buscaPorMatricula(1L);
        assertEquals(estudanteSalvar.getNome(), estudanteBuscado.getNome());
    }

    @Test
    void buscaPorMatriculaInvalidaTest() {
        NoResultException exception = assertThrows(NoResultException.class, () -> {
            estudanteService.buscaPorMatricula(1L);
        });
        assertEquals(NoResultException.class, exception.getClass());
    }

}