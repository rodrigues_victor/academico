package com.sicredi.academico.service;

import com.sicredi.academico.models.Estudante;
import com.sicredi.academico.models.Turma;
import com.sicredi.academico.repository.TurmaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class TurmaService {
    @Autowired
    private TurmaRepository repository;
    @Autowired
    private DiciplinaService diciplinaService;
    @Autowired
    private EstudanteService estudanteService;

    public Turma criarTurma(Turma turma) {
        validarTurma(turma);
        return repository.save(turma);
    }

    public List<Turma> findAll(){
        return repository.findAll();
    }

    public Turma adicionarEstudantes(Long id_turma, Set<Estudante> estudantes) {
        Turma turma = findTurmaById(id_turma);
        estudanteService.validarExistencia(estudantes);
        turma.setEstudantes(estudantes);
        return repository.save(turma);
    }

    public Set<Estudante> listarTodosEstudantes(Long turma_id) {
        Turma turma = findTurmaById(turma_id);
        return turma.getEstudantes();
    }

    public Turma findTurmaById(Long id) throws EntityNotFoundException {
        Optional<Turma> turma = repository.findById(id);

        if(turma.isEmpty()) {
            throw new EntityNotFoundException("Turma não existe");
        }

        return turma.get();
    }

    public void validarTurma(Turma turma) throws IllegalArgumentException {
        if (turma.getDiciplina() == null) {
            throw new IllegalArgumentException("diciplina vazia");
        }
        try {
            diciplinaService.findById(turma.getDiciplina().getId());
        } catch (EntityNotFoundException e) {
            throw new IllegalArgumentException("diciplina invalida");
        }
    }

}
