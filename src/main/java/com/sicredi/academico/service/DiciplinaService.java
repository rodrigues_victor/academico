package com.sicredi.academico.service;

import com.sicredi.academico.models.Diciplina;
import com.sicredi.academico.models.Estudante;
import com.sicredi.academico.repository.DiciplinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class DiciplinaService {
    @Autowired
    private DiciplinaRepository repository;
    @Autowired
    @Lazy
    private EstudanteService estudanteService;


    public Diciplina cadastrar(Diciplina diciplina) throws EntityExistsException {
        if (repository.findByNome(diciplina.getNome()).isPresent()) {
            throw new EntityExistsException("diciplina já existe");
        }
        return repository.save(diciplina);
    }

    public List<Diciplina> findAll() {
        return repository.findAll();
    }

    public Diciplina findById(Long id) throws EntityNotFoundException {
        Optional<Diciplina> diciplina = repository.findById(id);
        if(diciplina.isEmpty()) {
            throw new EntityNotFoundException();
        }
        return diciplina.get();
    }

    public Set<Estudante> listarTodosEstudantes(Long diciplina_id) {
        Diciplina diciplina = findById(diciplina_id);
        Set<Estudante> estudantes = new HashSet<>();
        diciplina.getEstudantesDiciplina()
                .forEach(
                        estudanteId -> estudantes.add(
                            estudanteService.buscaPorMatricula(estudanteId)
                        )
                );

        return estudantes;
    }

}
