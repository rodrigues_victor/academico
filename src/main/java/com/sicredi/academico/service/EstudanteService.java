package com.sicredi.academico.service;

import com.sicredi.academico.models.Diciplina;
import com.sicredi.academico.models.Estudante;
import com.sicredi.academico.repository.EstudanteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class EstudanteService {
    @Autowired
    private EstudanteRepository repository;
    @Autowired
    @Lazy
    private DiciplinaService diciplinaService;

    public Estudante salvarEstudante(Estudante estudante) {
        validarDados(estudante);

        estudante.setNome(estudante.getNome().toUpperCase());
        estudante.setEndereco(estudante.getEndereco().toUpperCase());

        return repository.save(estudante);
    }

    public List<Estudante> getAll() {
        return repository.findAll();
    }


    public Estudante buscaPorMatricula(Long matricula) throws NoResultException {
        Optional<Estudante> estudante = repository.findById(matricula);
        if (estudante.isEmpty()) {
            throw new NoResultException("matricula invalida");
        }
        return estudante.get();
    }

    public List<Estudante> buscaPorNome(String nome) {
        return repository.findByNomeContainingIgnoreCase(nome);
    }

    public Estudante buscaPorNumeroDocumento(Double numeroDoc) throws EntityNotFoundException {
        Optional<Estudante> estudante = repository.findByNumeroDocumento(numeroDoc);
        if (estudante.isPresent()) {
            return estudante.get();
        } else {
            throw new EntityNotFoundException("Estudante não existe");
        }
    }

    public Set<Diciplina> buscarDiciplinasMatriculadas(Long matricula_estudante) {
        Estudante estudante = buscaPorMatricula(matricula_estudante);
        Set<Diciplina> diciplinas = new HashSet<>();
        estudante.getDisciplinasId().forEach(diciplinaId -> diciplinas.add(diciplinaService.findById(diciplinaId)));
        return diciplinas;
    }

    public void validarDados(Estudante estudante) throws IllegalArgumentException {
        if (estudante.getNome().isEmpty()) {
            throw new IllegalArgumentException("nome não pode ser nulo");
        }
        if (estudante.getEndereco().isEmpty()) {
            throw new IllegalArgumentException("endereco não pode ser nulo");
        }
        if (estudante.getNumeroDocumento() == null) {
            throw new IllegalArgumentException("numero de documento não pode ser nulo");
        }
        if (repository.findByNumeroDocumento(estudante.getNumeroDocumento()).isPresent()) {
            throw new IllegalArgumentException("estudante com mesmo numero de documento já está cadastrado");
        }
    }

    public void validarDados(Set<Estudante> estudantes)  throws IllegalArgumentException {
        estudantes.forEach(estudante -> validarDados(estudante));
    }

    public void validarExistencia(Estudante estudante) {
        if (estudante.getMatricula() != null) {
            Optional<Estudante> estudanteOptional = repository.findById(estudante.getMatricula());
            if (estudanteOptional.isEmpty()) {
                throw new EntityNotFoundException("estudante não existe");
            }
        } else {
            throw new IllegalArgumentException("matricula nula");
        }
    }

    public void validarExistencia(Set<Estudante> estudantes) {
        estudantes.forEach(this::validarExistencia);
    }

}
