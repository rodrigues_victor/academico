package com.sicredi.academico.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Estudante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long matricula;
    @Column(unique = true)
    private Double numeroDocumento;
    private String nome;
    private String endereco;

    @ManyToMany(mappedBy = "estudantes" ,cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<Turma> turmas;

    private transient Set<Long> disciplinasId;

    public Estudante() {
    }

    public Estudante(String nome, String endereco, Double numeroDocumento) {
        this.nome = nome;
        this.endereco = endereco;
        this.numeroDocumento = numeroDocumento;
    }

    public Long getMatricula() {
        return matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Double getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(Double numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    @JsonBackReference
    public Set<Turma> getTurmas() {
        return turmas;
    }

    public void setTurmas(Set<Turma> turmas) {
        if(this.turmas == null) {
            this.turmas = new HashSet<>(turmas);
        }
        this.turmas.addAll(turmas);
    }

    @JsonIgnore
    public Set<Long> getDisciplinasId() {
        this.disciplinasId = new HashSet<>();
        if (this.turmas != null) {
            this.getTurmas().forEach(turma -> disciplinasId.add(turma.getDiciplina().getId()));
        }
        return disciplinasId;
    }
}
