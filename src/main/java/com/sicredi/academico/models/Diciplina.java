package com.sicredi.academico.models;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Diciplina {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;

    @JsonIgnore
    @OneToMany(mappedBy = "diciplina")
    private List<Turma> turmas;

    private transient Set<Long> estudantesId;

    public Diciplina() {
    }

    public Diciplina(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Turma> getTurmas() {
        return turmas;
    }

    @JsonIgnore
    public Set<Long> getEstudantesDiciplina() {
        this.estudantesId = new HashSet<>();
        if(this.turmas != null) {
            for (Turma turma : this.turmas) {
                turma.getEstudantes().forEach(estudante -> estudantesId.add(estudante.getMatricula()));
            }
        }
        return estudantesId;
    }
}
