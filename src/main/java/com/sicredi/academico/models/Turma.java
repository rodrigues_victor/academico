package com.sicredi.academico.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Turma {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Diciplina diciplina;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "estudante_turma",
            joinColumns = @JoinColumn(name = "turma_id"),
            inverseJoinColumns = @JoinColumn(name = "estudante_id")
    )
    private Set<Estudante> estudantes;

    public Turma() {
    }

    public Turma(Diciplina diciplina) {
        this.diciplina = diciplina;
    }

    public Long getId() {
        return id;
    }

    public Diciplina getDiciplina() {
        return diciplina;
    }

    public void setDiciplina(Diciplina diciplina) {
        this.diciplina = diciplina;
    }


    public Set<Estudante> getEstudantes() {
        return estudantes;
    }

    public void setEstudantes(Set<Estudante> estudantes) {
        if (this.estudantes == null) {
            this.estudantes = new HashSet<>(estudantes);
        }
        this.estudantes.addAll(estudantes);
    }
}
