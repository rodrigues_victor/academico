package com.sicredi.academico.repository;

import com.sicredi.academico.models.Estudante;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;
import java.util.Optional;


public interface EstudanteRepository extends JpaRepository<Estudante, Long> {

    List<Estudante> findByNomeContainingIgnoreCase(String nome);

    Optional<Estudante> findByNumeroDocumento(Double numeroDoc);
}
