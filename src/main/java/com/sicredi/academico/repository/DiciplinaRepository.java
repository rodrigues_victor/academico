package com.sicredi.academico.repository;

import com.sicredi.academico.models.Diciplina;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DiciplinaRepository extends JpaRepository<Diciplina, Long> {
    Optional<Diciplina> findByNome(String nome);

}
