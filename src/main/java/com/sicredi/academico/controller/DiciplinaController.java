package com.sicredi.academico.controller;

import com.sicredi.academico.models.Diciplina;
import com.sicredi.academico.models.Estudante;
import com.sicredi.academico.service.DiciplinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("diciplina")
public class DiciplinaController {
    @Autowired
    private DiciplinaService service;

    @GetMapping
    public ResponseEntity<List<Diciplina>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping("/listarEstudantes/{diciplina_id}")
    public ResponseEntity<Set<Estudante>> verTodosEstudantesTurma(@PathVariable Long diciplina_id) {
        return ResponseEntity.ok(service.listarTodosEstudantes(diciplina_id));
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<Diciplina> cadastrarDiciplina(@RequestBody Diciplina diciplina) {
        return ResponseEntity.ok(service.cadastrar(diciplina));
    }

}
