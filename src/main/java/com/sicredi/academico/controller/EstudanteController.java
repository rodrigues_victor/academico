package com.sicredi.academico.controller;

import com.sicredi.academico.models.Diciplina;
import com.sicredi.academico.models.Estudante;
import com.sicredi.academico.service.EstudanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("estudande")
public class EstudanteController {
    @Autowired
    private EstudanteService service;

    @GetMapping
    public ResponseEntity<List<Estudante>> verTodosEstudantes() {
        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<Estudante> salvarEstudante(@RequestBody Estudante estudante) {
        return ResponseEntity.ok(service.salvarEstudante(estudante));
    }

    @GetMapping("/matricula/{matricula}")
    public ResponseEntity<Estudante> buscarEstudantePorMatricula(@PathVariable Long matricula) {
        return ResponseEntity.ok(service.buscaPorMatricula(matricula));
    }

    @GetMapping("/nome/{nome}")
    public ResponseEntity<List<Estudante>> buscarEstudantePorNome(@PathVariable String nome) {
        return ResponseEntity.ok(service.buscaPorNome(nome));
    }

    @GetMapping("/diciplinasMatriculadas/{matricula}")
    public ResponseEntity<Set<Diciplina>> buscarDisciplinasMatriculadas(@PathVariable Long matricula) {
        return ResponseEntity.ok(service.buscarDiciplinasMatriculadas(matricula));
    }
}
