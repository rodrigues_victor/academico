package com.sicredi.academico.controller;

import com.sicredi.academico.models.Estudante;
import com.sicredi.academico.models.Turma;
import com.sicredi.academico.service.TurmaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("turma")
public class TurmaController {
    @Autowired
    private TurmaService service;

    @GetMapping("/listarEstudantes/{turma_id}")
    public ResponseEntity<Set<Estudante>> verTodosEstudantesTurma(@PathVariable Long turma_id) {
        return ResponseEntity.ok(service.listarTodosEstudantes(turma_id));
    }

    @GetMapping
    public ResponseEntity<List<Turma>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<Turma> criarTurma(@RequestBody Turma turma) {
        return ResponseEntity.ok(service.criarTurma(turma));
    }

    @PutMapping("/adicionar-estudante/{id_turma}")
    public ResponseEntity<Turma> adicionarEstudantes(
            @PathVariable Long id_turma,
            @RequestBody Set<Estudante> estudantes
    ) {
        return ResponseEntity.ok(service.adicionarEstudantes(id_turma, estudantes));
    }

}
